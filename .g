image: ubuntu

stages:
  - deploy

publish:
  stage: deploy
  tags: [ shell ]
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker build -t registry.gitlab.com/infektweb/docker/mailhog .
    - docker push registry.gitlab.com/infektweb/docker/mailhog
    - docker image rm registry.gitlab.com/infektweb/docker/mailhog
